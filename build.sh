#!/usr/bin/env bash
##########################################
# create a docker image from Docker file
# and run the docker
##########################################

#Prints an explanation of the tool
function help(){
	echo -e "This utility build a container that run restAPI for zerotier\n-b: To build docker image\n-r: To run the container\n-h: To help\n"
}
while getopts ":brh" arg; do
    case $arg in
        h) help;;
        b)  docker build -t restapi:v1 .
            echo "sucsess to build restapi IMAGE" ;;
        r)  IMAGE_ID=`docker images | grep restapi | awk '{print $3}'`
            echo "runs the docker... "
            sleep 3
            docker container run -d  -p 8080:8080 $IMAGE_ID
            echo "container is run" ;;
    esac
done

